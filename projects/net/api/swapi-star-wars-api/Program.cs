﻿using System;
using System.Net;
using System.IO; // Stream
using Newtonsoft.Json; // JsonConvert Class

namespace swapi_star_wars_api {

    class Planet {
        public string name;
        public string rotation_period;
    }

    class Program {        

        static void Main(string[] args) {            
            var content = string.Empty;

            // Realizamos llamada             
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://swapi.co/api/planets/1/");
            
            // Leemos la respuesta
            try {
                var response = (HttpWebResponse)request.GetResponse();
            
                if (response.StatusCode == HttpStatusCode.OK) {
                   Console.WriteLine("HTTP status ok");     
                   var stream = response.GetResponseStream();
                   var sr = new StreamReader(stream);
                   
                   content = sr.ReadToEnd();
                   var planetData = JsonConvert.DeserializeObject<Planet>(content);                   
                   Console.WriteLine(planetData.name);
                   Console.WriteLine(planetData.rotation_period);                   
                   
                } else {
                    Console.WriteLine("HTTP status code Error!");
                }


            } catch (WebException e) { // Capturamos excepción 
                using (WebResponse response = e.Response) {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Web exception error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream()) {
                        string text = new StreamReader(data).ReadToEnd();
                        
                        Console.WriteLine(text);
                    }
                    response.Close();
                }
            } finally {
                //
            }


            Console.ReadLine();
        }
    }
}
