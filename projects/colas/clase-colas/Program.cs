﻿using System;

namespace clase_colas
{

    class Nodo  {
        public int info;
        public Nodo sig;
    }

    class Cola {
        private Nodo raiz,fondo;

        public Cola() {
            raiz=null;
            fondo=null;
        }

        public bool isVacia () {
            if (raiz == null)
                return true;
            else
                return false;
        }

        public void insertarNodo (int info) {
            Nodo nuevo;
            nuevo = new Nodo ();
            nuevo.info = info;
            nuevo.sig = null;
            if (isVacia ())  {
                raiz = nuevo;
                fondo = nuevo;
            } else {
                fondo.sig = nuevo;
                fondo = nuevo;
            }
        }

        public int extraerNodo () {
            if (!isVacia ()) {
                int informacion = raiz.info;
                if (raiz == fondo) {
                    raiz = null;
                    fondo = null;
                } else {
                    raiz = raiz.sig;
                }
                return informacion;
            }
            else
                return int.MaxValue;
        }

        public void imprimirCola() {
            Nodo reco=raiz;
            Console.WriteLine("Listado de todos los elementos de la cola.");
            while (reco!=null)  {
                Console.Write(reco.info+"-");
                reco=reco.sig;
            }
            Console.WriteLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Cola miCola=new Cola();

            miCola.insertarNodo(5);
            miCola.insertarNodo(10);
            miCola.insertarNodo(50);
            miCola.imprimirCola();
            Console.WriteLine("Extraemos uno de la cola:"+miCola.extraerNodo());
            miCola.imprimirCola();
            Console.ReadKey();
 
        }
    }
}
