﻿using System;
using desc_get_set.clases; // <--- Acceso a LibroCalificaciones

namespace desc_get_set {
    class Program {
        
        static void Main(string[] args) {            
            LibroCalificaciones librocal = new LibroCalificaciones(); // <---Instancia a nueva clase 
            string miCurso = "";

            librocal.NombreCurso = "Angular";  // <--- Probamos desc acceso set

            Console.WriteLine("Bienvenido al curso de: "+librocal.NombreCurso); // <--- Probamos desc acceso get                

            librocal.muestraCurso();

            miCurso = librocal.dameCurso();

            Console.WriteLine("Mi curso es: "+miCurso);

            librocal.NombreCurso = "";

            Console.ReadLine();
        }
    }
}
