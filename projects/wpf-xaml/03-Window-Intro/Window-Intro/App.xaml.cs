﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Window_Intro
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e) {            
            MainWindow wnd = new MainWindow();            
            wnd.Title = "Something else";
            //if (e.Args.Length == 1) MessageBox.Show("Now opening file: \n\n" + e.Args[0]);
            if (e.Args.Length == 1) // <---- NUEVO
                wnd.TestArg(e.Args[0]);
            wnd.Show(); // Show the window
        }


        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e) {
            MessageBox.Show("App.xaml.cs: An unhandled exception just occurred: " + e.Exception.Message, "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
            e.Handled = true;
        }


    }
}
