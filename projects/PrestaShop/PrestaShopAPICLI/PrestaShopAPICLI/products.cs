﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml;
using System.Xml.Serialization;
using System.Net;
using System.IO;

namespace PrestaShopAPICLI
{
    public class ProductsItem {
        private string Id;
        private string Xlink;
        
        /**
         * Constructor de la clase recibe identificador del artículo 
         **/
        public ProductsItem(string Id,string Xlink) {
            if (string.IsNullOrEmpty(Id)) {
                throw new ArgumentException("message", nameof(Id));
            }
            this.Id = Id;
            this.Xlink = Xlink;
        }
    } // FIN class ProductsItem

    //------------------------------------------------------------------------

    class Products {
        private string PrestaShopAPIUri;
        private string PasswordToken;
        public List<ProductsItem> ProductsList = new List<ProductsItem>();

        /**
         * Constructor de la clase, recibe como parámetros dos cadenas de texto con la URL de la API webservice de 
         * PrestaShop y la clave generada del webservice en el backend de admin de PrestaShop.
         **/
        public Products(string PrestaShopAPIUri, string PasswordToken) {
            this.PrestaShopAPIUri = PrestaShopAPIUri;
            this.PasswordToken = PasswordToken;
        }
        
        //------------------------------------------------------------------------

        /**
         * Realiza consulta XPATH sobre una cadena y retorna nodos resultantes
         **/
        private XmlNodeList XPathQuery(string XMLInp, string XPath) {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(XMLInp);

            XmlNodeList nodes = xmlDoc.SelectNodes(XPath);

            return nodes;
        } // FIN XPathQuery

        //-------------------------------------------------------------------------------

        /**
         * Método público para lanzar la consulta, retorna número de productos encontrados.
         **/
        public int GetProducts() {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.PrestaShopAPIUri);
            NetworkCredential nc = new NetworkCredential(this.PasswordToken, "");
            var content = string.Empty;
            string xpath = "prestashop/products/product";

            request.Credentials = nc;
            request.Method = "GET";

            try {
                using (var response = (HttpWebResponse)request.GetResponse()) {
                    //Console.WriteLine("Content type: {0}, encoding: {1}, length: {2}.", response.ContentType, response.ContentEncoding, response.ContentLength);
                    if (response.StatusCode == HttpStatusCode.OK) {
                        using (var stream = response.GetResponseStream()) {
                            using (var sr = new StreamReader(stream)) {
                                content = sr.ReadToEnd();
                                /*XmlDocument xmlDoc = new XmlDocument();
                                xmlDoc.LoadXml(content);
                                var nodes = xmlDoc.SelectNodes(xpath);*/

                                /*
                                Si quisiesmos guardar en fichero local como traza  
                                var myUniqueFileName = $@"{DateTime.Now.Ticks}.xml";
                                File.WriteAllText(myUniqueFileName, content);
                                */
                                //xmlDoc.Load("myUniqueFileName.xml");                                                                

                                var nodes = this.XPathQuery(content, xpath);
                                //Console.WriteLine("Count {0}.", nodes.Count);

                                foreach (XmlNode childrenNode in nodes) {
                                    //Console.WriteLine("Attribute id value: {0}", childrenNode.Attributes["id"].Value);
                                    ProductsItem Item = new ProductsItem(childrenNode.Attributes["id"].Value,
                                        childrenNode.Attributes["xlink:href"].Value);
                                    this.ProductsList.Add(Item);
                                }
                                sr.Close();
                                response.Close(); //cerrar la respuesta y liberar la conexión para su reutilización
                            }
                        }
                    } else {
                        //Console.WriteLine("Error status code {0}", response.StatusCode); // Error 
                    }
                }
            } catch (WebException e) { // Capturamos excepción 
                using (WebResponse response = e.Response) {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    //Console.WriteLine("Web exception error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream()) {
                        string text = new StreamReader(data).ReadToEnd();
                        //Console.WriteLine(text);
                    }
                    response.Close();
                }
            } finally {
                //
            }

            return this.ProductsList.Count; // Retorna el número de elementos incluidos en List<T>.
        } // FIN GetProducts
    } // FIN Products
}
