# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## 2019-03-13
### Added
- New simplified registration form for free users @evanagee

### Changed
- Solucionado problema XPATH no retornaba nodos, era problema de parámetros de entrada a compilación URL a Webservice estaba mal.