﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace PrestaShopAPICLI
{
    
    /*
    public class Product {
        //[XmlElement("id")]
        //public long Id { get; set; }

        [XmlAttribute("id")]
        public int Id { get; set; }
    }

    [XmlRoot(ElementName = "products")]
    public class Products {
        [XmlElement("product")]
        public List<Product> listOfProducts { get; set; }

    }

    [XmlRoot(ElementName = "prestashop")]
    public class Prestashop
    {
        [XmlElement(ElementName = "products")]
        public Products prods;
        //[XmlAttribute(AttributeName = "xlink", Namespace = "http://www.w3.org/2000/xmlns/")]
        //public string Xlink { get; set; }
    } */

    //------------------------------------------------------------------

    public class ProductLst {
        
    } // FIN class GetProductLst

    //----------------------------------------------------------------------------------

    class Program
    {        
        static void Main(string[] args)
        {
            Products MyLst = new Products(args[0], args[1]);
            MyLst.GetProducts();
            Console.WriteLine("List returns: {0}.",MyLst.ProductsList.Count);
        }
    }
}
