[TOC]

# Redis

[**Redis**](https://redis.io/) (REmote DIctionary Server) es un sistema de almacenamiento en memoria volátil en RAM basado en registros clave-valor NoSQL (opcionalmente podemos hacer que sea persistente o durable). También soporta la replicación de datos maestro-esclavo. Actualmente Redis es usado por: Twitter, Instagram, GitHub, Flickr, o Digg.

# ¿Pero en que casos puede ser necesario un sistema así?

Por ejemplo para el análisis y procesamiento de datos en tiempo real, supongamos que obtenemos muestreos periódicos de un sensor y cada nueva lectura vuelve a calcular un valor basándonos en las lecturas previas, en ese caso un sistema de gestión de base de datos clásico como MySQL realizando transacciones a disco puede no ser muy eficiente comparado con Redis (tiempos de respuesta menores).

Un sistema de comunicaciones basado en mensajería por ejemplo.

Redis incluye clientes para la mayoría de los lenguajes de programación conocidos (PHP y C# entre ellos claro).

Redis está escrito en C y está ideado para sistemas Linux, aún así existe un desarrollo para sistemas MS Windows.

# Instalación en Windows

Podemos descargar versiones binarias del instalador del siguiente enlace en GitHub. La versión que he descargado es Redis-x64-3.2.100.zip.

El paquete apenas ocupa 5MB. Una vez descomprimido tenemos todos los binarios y utilidades básicas necesarias.

Abrimos una consola DOS y ejecutamos redis-server.exe.

Una vez ejecutado la consola nos muestra información valiosa, se está ejecutando a la escucha en el puerto 6379, para que ejecute el fichero de configuración copiamos el fichero redis.windows.conf a redis.conf y ejecutamos:

```
C:\> redis-server.exe redis.conf
```

Redis incluye la utilidad redis-benchmark.exe para poner a prueba su velocidad, simula la ejecución de comandos de forma simultanea desde varios clientes.

# Cliente en C

# Enlaces internos

- ikerlandajuela.wordpress.com ["Redis: Almacenamiento de datos NoSQL clave-valor en memoria"](https://ikerlandajuela.wordpress.com/2017/07/23/redis-almacenamiento-de-datos-clave-valor-en-memoria/).

# Enlaces externos

- dzone.com ["Introduction to Message Brokers (Part 2): ActiveMQ vs. Redis Pub/Sub"](https://dzone.com/articles/introduction-to-message-brokers-part-2-activemq-vs).
- dzone.com ["Introduction to Message Brokers. Part 1: Apache Kafka vs. RabbitMQ"](https://dzone.com/articles/introduction-to-message-brokers-part-1-apache-kafk).
- ["ServiceStack/ServiceStack.Redis"](https://github.com/ServiceStack/ServiceStack.Redis): .NET's leading C# Redis Client [https://servicestack.net/redis](https://servicestack.net/redis).

- [jkruer01/EasyRedisMQ](https://github.com/jkruer01/EasyRedisMQ) : A Simple .Net Message Queue that Uses Redis for the back end.
- [ServiceStack/ServiceStack.Redis](https://github.com/ServiceStack/ServiceStack.Redis): .NET’s leading C# Redis Client.
- [ServiceStackApps/RedisReact](https://github.com/ServiceStackApps/RedisReact): Redis Browser in React.
- [Redis MQ.Redis for .NET Developers](http://taswar.zeytinsoft.com/redis-pub-sub/): Redis Pub Sub.
- [Domain Events – Redis PubSub – Subscriptions](https://pdalinis.blogspot.com.es/2013/05/domain-events-redis-pubsub-subscriptions.html).
- [Redis on Windows](http://geekswithblogs.net/shaunxu/archive/2012/04/27/redis-on-windows.aspx).
- https://andrew-bn.github.io/RedisBoost/
-https://github.com/erikdubbelboer/phpRedisAdmin
