[TOC]

# Como es una aplicación WPF

## Window

El primer elemento de una aplicación es la ventana o la clase _Window_, es el raíz de la ventana y contiene algunos elementos como un borde, una barra de título y los botones de control de la ventana.

```XAML
<Window x:Class="_02_MouseUp_basic_evt.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:_02_MouseUp_basic_evt"
        mc:Ignorable="d"
        Title="MainWindow" Height="450" Width="800">
    <Grid></Grid>
</Window>
```

El atributo `x:class` le dice al XAML que clase usar, en este caso es la clase `MainWindow`. La clase está declarada como `partial` porque se combina con el archivo XAML en tiempo de ejecución para crear la ventana usando la función `InitializeComponent` en el constructor de la clase.

Dentro del XAML tambien definimos algunos atributos como el título, altura y anchura de la ventana. VS añade una rejilla o _grid_ de forma automática a nuestra aplicación.

Las propiedades más importantes de la clase [**`Window`**](https://docs.microsoft.com/en-us/dotnet/api/system.windows.window?view=netframework-4.8):

- **Icon**: Icono de la ventana.
- **ResizeMode**: Permite al usuario redimensionar la ventana (por defecto es CanResize). Otros valores posibles son CanMinimize o NoResize.
- **ShowInTaskbar**: Por defecto es `true` para que aparezca en la barra de tareas, el caso contrario puede ser útil para ventanas secundarias o aplicaciones que se minimizan en la bandeja.
- **SizeToContent**: Indica si la ventana debe redimensionarse al contenido. Por defecto es `Manual`. Otras opciones son Width: Ancho, Height: Alto y WidthAndHeight: Ancho y Alto.
- **Topmost**: Si se pone a `true` tu ventana se muestra encima de las demás excepto que este minimizada.
- **WindowStartupLocation**: Posición inicial de la ventana. Por defecto es `Manual` (usa propiedades `Top`y `left`). `CenterOwner` la posiciones en el centro de la ventana que lo contenga y `CenterScreen` en el centro de la pantalla.
- **WindowState**: Estado inicial de la ventana. Puede ser Normal (por defecto), Maximized (maximizada) o Minimized (minimizada).

# App.xaml

El fichero "App.xaml" es el punto declarativo de entrada a nuestra aplicación (VS lo crea por defecto al crear el proyecto junto con el _code-behind_ "App.xaml.cs").

```XAML
<Application x:Class="Window_Intro.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:local="clr-namespace:Window_Intro"
             StartupUri="MainWindow.xaml">
    <Application.Resources>

    </Application.Resources>
</Application>
```

```CSharp
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Window_Intro
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
```

"App.xaml.cs" [hereda](https://docs.microsoft.com/es-es/dotnet/csharp/tutorials/inheritance) su clase parcial (divide una clase en varios archivos fuente) `App` de la clase `Application`, esta clase es el lugar para suscribir eventos importantes (por ejemplo el inicio de la aplicación) o recursos globales usados por toda la aplicación.

El atributo `StartupUri` del XAML indica que ventana o página usar para iniciar la aplicación, por defecto es "MainWindow.xaml" (también se puede borrar esta propiedad y hacerlo desde el código).

# Evento Startup en XAML para crear la ventana inicial

Reemplazamos el atributo `StartupUri` por `Startup`:

```XAML
<Application x:Class="Window_Intro.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:local="clr-namespace:Window_Intro"
             Startup="Application_Startup" >
             <!-- StartupUri="MainWindow.xaml" > -->
    <Application.Resources>

    </Application.Resources>
</Application>
```

Creamos el método `Application_Startup` en la clase `App`:

```CSharp
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Create the startup window
            MainWindow wnd = new MainWindow();
            // Do stuff here, e.g. to the window
            wnd.Title = "Something else";
            // Show the window
            wnd.Show();
        }
```

El método que suscribimos al evento sigue la misma lógica que hemos visto en otras ocasiones, recibe como parámetros un objeto con el control y información adicional. Lo interesante es **que podemos modificar la ventana a nuestro antojo desde el código** (en este caso cambiamos el título).

![](img/01.PNG)

# Argumentos de CLI

Los argumentos por CLI (Command Line Interface) en la llamada a nuestra aplicación se envían a través del evento `Startup` como argumento del evento (`StartupEventArgs`).

```CSharp
        private void Application_Startup(object sender, StartupEventArgs e) {
            MainWindow wnd = new MainWindow();
            wnd.Title = "Something else";
            if (e.Args.Length == 1) MessageBox.Show("Now opening file: \n\n" + e.Args[0]);
            wnd.Show(); // Show the window
        }
```

La nueva línea comprueba si ha llegado algún argumento con una sentencia condicional, en caso afirmativo muestra una ventana emergente con la información.

Yo he preparado un pequeño script PS (PowerShell) para ejecutar la aplicación:

```PS
.\Window-Intro.exe Argumento
```

Cuando lanzamos la aplicación se muestra la ventana emergente `MessageBox` antes incluso de que se vea la ventana principal, además la aplicación no continua cargando la ventana principal mientras no cerremos el `MessageBox`.

![](img/02.PNG)

Podemos pasar los argumentos sin salir del IDE de VS en el menú Proyecto > Propiedades.

![](img/03.PNG)

En lugar de tratar el argumento en esta clase tal vez queramos mandarlo al constructor de la ventana inicial en "MainWindow.xaml.cs".

App.xml.cs:

```CSharp
        private void Application_Startup(object sender, StartupEventArgs e) {
            MainWindow wnd = new MainWindow();
            wnd.Title = "Something else";
            //if (e.Args.Length == 1) MessageBox.Show("Now opening file: \n\n" + e.Args[0]);
            if (e.Args.Length == 1) // <---- NUEVO
                wnd.TestArg(e.Args[0]);
            wnd.Show(); // Show the window
        }
```

MainWindow.xaml.cs:

```CSharp
        public void TestArg(string argumentos) {
            MessageBox.Show("MainWindow - arguments:\n\n" + argumentos);
        }
```

# Manejando excepciones

Vamos a forzar una excepción:

```chsarp
        private void PnlMainGrid_MouseDown(object sender, MouseButtonEventArgs e) {
            string s = null;
            s.Trim();
        }
```

![](img/04.PNG)

Definimos un bloque `try { } catch {}` para gestionar el error por nosotros mismos desde el código:

```R
            try {
                s.Trim();
            } catch (Exception ex) {
                MessageBox.Show("A handled exception just occurred: " + ex.Message, "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
```

![](img/05.PNG)

WPF permite manejar todas las excepciones que no manejamos desde el propio código donde se producen mediante el evento `DispatcherUnhandledException` en la clase `Application`.

En App.xaml añadimos una nueva línea:

```XAML
<Application x:Class="Window_Intro.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:local="clr-namespace:Window_Intro"
             DispatcherUnhandledException="Application_DispatcherUnhandledException"
             Startup="Application_Startup" >
             <!-- StartupUri="MainWindow.xaml" > -->
</Application>
```

En App.xaml.cs:

```CSharp
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("An unhandled exception just occurred: " + e.Exception.Message, "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Warning);
            e.Handled = true;
        }
```

En la función `Window_MouseDown` añadimos una nueva línea fuera del bloque try-catch, el resultado es este cuando se produce:

![](img/06.PNG)

La propiedad `e.Handled` en verdadero. Esto le informa a WPF que terminamos con el manejo de la excepción y no se debe realizar nada más al respecto.

# El control TextBlock

El control _TextBlock_ no hereda de la clase _Control_ pero se comporta de forma similar. _TextBlock_ permite colocar texto en la ventana (como un _label_ pero normalmente para textos más largos de varias líneas).

Añadimos el elemento en MainWindow.xaml:

```XAML
        <TextBlock Margin="10" Foreground="Red">
            This is a TextBlock control<LineBreak />
            and it comes with a very long text
        </TextBlock>
        <TextBlock Margin="10" TextTrimming="CharacterEllipsis" Foreground="Green">
			This is a TextBlock control with text that may not be rendered completely, which will be indicated with an ellipsis.
        </TextBlock>
        <TextBlock Margin="10" TextWrapping="Wrap" Foreground="Blue">
			This is a TextBlock control with automatically wrapped text, using the TextWrapping property.
        </TextBlock>
```

En los bloques de texto de arriba he jugado con el color de letra, el margen, la etiqueta `LineBreak` para introducir un salto de línea manual.

La propiedad `TextTrimming="CharacterEllipsis"` hace que el bloque de texto acabe en (...) cuando no puede ajustarlo.

La propiedad `TextWrapping="Wrap"` produce el ajuste automático de línea.

# TextBlock - Formato de texto

El formato de texto se puede definir en línea para porciones de texto: AnchoredBlock (bloque anclado), Bold (negrita), Hyperlink (hipervínculo), InlineUIContainer (contenedor de controles), Italic (cursiva), LineBreak (salto de línea), Run (texto con formato), Span (texto y elementos con formato) y Underline (subrayado).

```XAML
        <TextBlock Margin="10" TextWrapping="Wrap">
			TextBlock with <Bold>bold</Bold>, <Italic>italic</Italic> and <Underline>underlined</Underline> text.
        </TextBlock>
```

# Código fuente

- csharp / projects / wpf-xaml / [03-Window-Intro](https://gitlab.com/soka/csharp/tree/master/projects/wpf-xaml/03-Window-Intro).

# Enlaces externos

- ["El control TextBlock - Formato en línea"](https://wpf-tutorial.com/es/15/controles-basicos/el-control-textblock-formato-en-linea/).
- ["How to bind a string from a resource file to a WPF/XAML window"](https://scottlilly.com/how-to-bind-a-string-from-a-resource-file-to-a-wpfxaml-window/).
- docs.microsoft.com ["Clases y métodos parciales (Guía de programación de C#)"](https://docs.microsoft.com/es-es/dotnet/csharp/programming-guide/classes-and-structs/partial-classes-and-methods)
- docs.microsoft.com ["inheritance"](https://docs.microsoft.com/es-es/dotnet/csharp/tutorials/inheritance)
- Clase .Net ["Window"](https://docs.microsoft.com/en-us/dotnet/api/system.windows.window?view=netframework-4.8).
- wpf-tutorial.com ["Working with App.xaml"](https://wpf-tutorial.com/es/10/una-aplicacion-wpf/working-with-app-xaml/).
- ["Resources in WPF and difference between Static Resource and Dynamic Resource"](https://www.dotnetcurry.com/wpf/1142/resources-wpf-static-dynamic-difference).
